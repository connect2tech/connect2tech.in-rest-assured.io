package in.connect2tech.student;


import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;

public class TestBase{
	@BeforeClass
	public void init() {
		RestAssured.baseURI = "http://localhost:8080/student";
	}
}
