package in.connect2tech.student;

import static io.restassured.RestAssured.given;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class GetStudents {
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "http://localhost:8080/student";
		RestAssured.basePath = "";
	}

	
	@Test
	public void getAllStudents() {
		Response response = 
				given().
					get("/list");
		
		//Formatted data
		System.out.println("response.prettyPrint()=====>"+response.prettyPrint());
		System.out.println("response.body().asString()=====>"+response.body().asString());
		//JsonPath jsPath = new JsonPath(response.prettyPrint());
		
	}
	
	@Test
	public void getOneStudents() {
		Response response = 
				given().
					get("/1");
		
		//Formatted data
		System.out.println("response.prettyPrint()=====>"+response.prettyPrint());
		System.out.println("response.body().asString()=====>"+response.body().asString());
	
	}
}