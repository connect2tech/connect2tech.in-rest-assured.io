package in.connect2tech.reqres.apis.get;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static org.hamcrest.Matchers.equalTo;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class GetCallsResponseValidation2 {
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://reqres.in/";
		RestAssured.basePath = "/api";
	}
	
	
	@Test
	public void validateResponseStatusValueContent2() {
				given().
				when().
					get("/users/1").
				then().statusCode(200).
					and().
					body("data.id", equalTo(1)).
					and().
					contentType(ContentType.JSON).log().all();
	}

}