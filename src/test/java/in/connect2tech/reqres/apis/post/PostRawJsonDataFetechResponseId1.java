package in.connect2tech.reqres.apis.post;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import javafx.beans.binding.When;

import static org.hamcrest.Matchers.equalTo;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PostRawJsonDataFetechResponseId1 {
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://reqres.in/";
		RestAssured.basePath = "/api";
	}

	@Test
	public void postData() {
		String jsonBody = "{" + "\"name\": \"morpheus11\"," + "\"job\": \"leader11\"" + "}";
				given().
						body(jsonBody)
				.when().
					post("/users").
				then().
					statusCode(201).log().all();
		
	}
	
	@Test
	public void postDataGetId() {
		String jsonBody = "{" + "\"name\": \"morpheus11\"," + "\"job\": \"leader11\"" + "}";
				Response response = 
				given().
						body(jsonBody)
				.when().
					post("/users");
				
				String id = response.path("id");
				System.out.println(id);
		
	}
}