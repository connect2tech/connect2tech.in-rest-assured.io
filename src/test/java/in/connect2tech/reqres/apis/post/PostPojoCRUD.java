package in.connect2tech.reqres.apis.post;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import javafx.beans.binding.When;

import static org.hamcrest.Matchers.equalTo;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PostPojoCRUD {
	
	String id;
	
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://reqres.in/";
		RestAssured.basePath = "/api";
	}

	@Test(priority=1)
	public void createUser() {

		User user = new User();
		user.setJob("Automation Architect");
		user.setName("Cognizant");

		Response response = 
				given().
					body(user).
				when().
					post("/users");

		System.out.println(response.body().prettyPrint());

		id = response.path("id");
		System.out.println("id1==>"+id);
		

	}
	
	@Test(priority=2)
	public void retrieveUser() {
		
		String userId = "/users/"+ "1";
		
				given().
				when().
					get(userId).
				then().statusCode(200).
					and().
					body("data.id", equalTo(1)).
					and().
					contentType(ContentType.JSON).log().all();
	}
	
	
}