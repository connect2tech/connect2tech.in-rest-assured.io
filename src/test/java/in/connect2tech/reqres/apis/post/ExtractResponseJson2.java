package in.connect2tech.reqres.apis.post;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ExtractResponseJson2 {

	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://reqres.in/";
		RestAssured.basePath = "/api";
	}

	@Test
	public void extractResponse() {

		User user = new User();
		user.setJob("Automation Architect");
		user.setName("Cognizant");

		/*Response response = given().body(user).when().post("/users");
		System.out.println("response.body().prettyPrint()=>"+response.body().prettyPrint());
		String id = response.path("id");
		System.out.println(id);*/

		Response response = given().body(user).when().post("/users").then().statusCode(201).extract().response();
		System.out.println("response.body().prettyPrint()=>"+response.body().prettyPrint());
		String id = response.path("id");
		System.out.println(id);
		
		String responseString = response.asString();
		System.out.println(responseString);

		JsonPath jsPath = new JsonPath(responseString);
		String json_id = jsPath.get("id");
		System.out.println("The json_id is: " + json_id);

				
	}
}