package in.connect2tech.google.distance.matrix.api;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import static io.restassured.RestAssured.given;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import static org.hamcrest.Matchers.*;

public class GetRequestJsonHardAssertion {
	/***
	 * Given I have this information When I perform this action Then this should
	 * be the output
	 */
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://maps.googleapis.com";
		RestAssured.basePath = "/maps/api";
	}
	
	@Test
	public void getResponseBody() {
		given().param("units", "imperial").param("origins", "Washington,DC")
				.param("destinations", "New+York+City,NY").param("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8")
				.when().get("/distancematrix/json").then().statusCode(200)
				.body("rows[0].elements[0].distance.text", equalTo("226 mi"))
				.body("rows[0].elements[0].size()", equalTo(3))
				.body("rows[0].size()", equalTo(1))
				.body("destination_addresses", hasItem("New York, NY, USA"))
				;
	}
}