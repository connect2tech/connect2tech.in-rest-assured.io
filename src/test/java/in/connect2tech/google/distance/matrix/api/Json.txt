{
   "destination_addresses" : [ "New York, NY, USA" ],
   "origin_addresses" : [ "Washington, DC, USA" ],
   "rows" : [
      {
         "elements" : [
            {
               "distance" : {
                  "text" : "226 mi",
                  "value" : 364018
               },
               "duration" : {
                  "text" : "3 hours 53 mins",
                  "value" : 14000
               },
               "status" : "OK"
            }
         ]
      }
   ],
   "status" : "OK"
}