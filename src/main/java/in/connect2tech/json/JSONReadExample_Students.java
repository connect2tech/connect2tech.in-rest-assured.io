package in.connect2tech.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasValue;

//Java program to read JSON from a file 

import java.io.FileReader;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

public class JSONReadExample_Students {
	public static void main(String[] args) throws Exception {
		// parsing file "JSONExample.json"
		Object obj = new JSONParser().parse(new FileReader("Students.json"));

		// typecasting obj to JSONObject
		JSONArray jo = (JSONArray) obj;

		Map <String, Object> m = (Map) jo.get(jo.size() - 1);

		System.out.println(m);

		assertThat(m, hasValue("firstName1"));
	}
}
